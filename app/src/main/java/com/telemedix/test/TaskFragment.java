package com.telemedix.test;

import android.app.Activity;
import android.app.Fragment;

import android.content.Context;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;

import android.support.annotation.CallSuper;

import android.telephony.TelephonyManager;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;

import static com.telemedix.test.MainActivity.TAG;

public class TaskFragment extends Fragment
{
	@CallSuper
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		
		Activity activity = getActivity();
		TelephonyManager telephonyManager = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
		deviceId = telephonyManager.getDeviceId();
		url = activity.getString(R.string.url);
		if(BuildConfig.DEBUG) Log.d(TAG, "deviceId: "+deviceId+" URL: "+url);
	}
	
	@CallSuper
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		if(task != null) task.cancel(true);
	}
	
	public void startTask()
	{
		if(task != null && task.isRunning()) return;
		task = new MyTask(this);
		task.execute();
	}
	
	public Answer getAnswer()
	{
		return answer;
	}
	
	public void setAnswer(Answer answer)
	{
		this.answer = answer;
		if(isAdded() && getActivity() != null) ((MainActivity)getActivity()).answer(answer);
	}
	
	public String getDeviceId()
	{
		return deviceId;
	}
	
	public String getUrl()
	{
		return url;
	}
	
	private MyTask task;
	private String deviceId;
	private String url;
	private Answer answer;
	
	public static final MediaType CONTENT_TYPE = MediaType.parse("application/x-www-form-urlencoded");
	public static final String PARAM_ID = "id";
	
	private static class MyTask extends AsyncTask<Void, Answer, Void>
	{
		public MyTask(TaskFragment taskFragment)
		{
			this.taskFragment = taskFragment;
		}
		
		@Override
		protected void onPreExecute()
		{
			running = true;
		}
		
		@Override
		protected Void doInBackground(Void... nothing)
		{
			OkHttpClient client = new OkHttpClient();
			Request request = new Request.Builder()
				.url(taskFragment.getUrl())
				.post(RequestBody.create(CONTENT_TYPE, PARAM_ID+"="+taskFragment.getDeviceId()))
				.build();
			String body = null;
			try
			{
				Response response = client.newCall(request).execute();
				body = response.body().string();
			}
			catch(IOException e)
			{
				publishProgress(new Answer(e.getMessage()));
				return null;
			}
			if(BuildConfig.DEBUG) Log.d(TAG, "response:\n"+body);
			Gson gson = new GsonBuilder().create();
			SystemClock.sleep(5000);
			publishProgress(gson.fromJson(body, Answer.class));
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Answer... answers)
		{
			taskFragment.setAnswer(answers[0]);
		}
		
		@Override
		protected void onPostExecute(Void nothing)
		{
			running = false;
		}
		
		@Override
		protected void onCancelled()
		{
			running = false;
			taskFragment.setAnswer(null);
		}
		
		public boolean isRunning()
		{
			return running;
		}
		
		private TaskFragment taskFragment;
		private boolean running;
	}
}

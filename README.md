## Тестовое задание для Телемедикс ##

Design and build android activity for serving full screen html ads.
When Activity is shown request for ad by sending HTTP POST request with parameter id with value of sim card IMSI (subscriber id) http://www.505.rs/adviator/index.php

Example:


```
#!shell
POST /adviator/index.php HTTP/1.1
Host: www.505.rs
Cache-Control: no-cache
Postman-Token: abd93bb8-2857-2fd0-7679-0b25087e1d35
Content-Type: application/x-www-form-urlencoded

id=85950205030644900

{
"status":"OK",
"message":"display full screen ad",
"url":"http://www.505.rs/adviator/ad.html"
}
```
If status is equal "OK" use returned "url" and load ad into Activity webView.
If status is not equal "OK" show dialog with 'message' text and OK button. Clicking OK button will dismiss both dialog and activity.
While requesting for ad and loading ad html show spinner in center of screen and transparent background.
Once html ad is loaded hide spinner and show ad.
When user clicks ad link close activity and open native android browser with clicked url.
Activity should work in both portrait and landscape mode.

#### Сборка и установка ####
```
#!shell
$./gradlew clean installDebug
```

#### Сборки ####
[Неподписаный релиз](https://bitbucket.org/Tepex/telemedix-test/downloads/app-release-unsigned.apk)

[Отладочная версия](https://bitbucket.org/Tepex/telemedix-test/downloads/app-debug.apk)